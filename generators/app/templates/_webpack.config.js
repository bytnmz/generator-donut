const PACKAGE = require('./package.json');
const assetPath = PACKAGE.assetPath;

const path = require('path');
const fs = require('fs')
const webpack = require('webpack');
const multiJsonLoader = require('multi-json-loader');

const HappyPack = require('happypack');
const happyThreadPool = HappyPack.ThreadPool({ size: 4 });
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const SpeedMeasurePlugin = require("speed-measure-webpack-plugin");

console.log('Running development build......');

// Create page data
const siteData = multiJsonLoader.loadFiles('./src/_data');

// Recursively created nested JSON object based on the folder structure in the folder
generateJSON('./src/_data', siteData);

function generateJSON(startPath, parentObj) {
  const files = fs.readdirSync(startPath);

  for(let i = 0; i < files.length; i++){
    const filename = path.join(startPath,files[i]);
    const stat = fs.lstatSync(filename);

    if (stat.isDirectory()){
      parentObj[`${files[i]}`] = multiJsonLoader.loadFiles(filename);
      generateJSON(filename, parentObj[`${files[i]}`]);
    }
  }
}

function getIndexPugFiles(startPath,filter){
  let results = [];

  if (!fs.existsSync(startPath)){
      console.log("no dir ",startPath);
      return;
  }

  const files = fs.readdirSync(startPath);
  for(let i = 0; i < files.length; i++){
    const filename = path.join(startPath,files[i]);
    const stat = fs.lstatSync(filename);
    if (stat.isDirectory()){
      // recursively created nested array of file names
      results = results.concat(getIndexPugFiles(filename,filter));
    }
    else if ((filename.indexOf(filter) >= 0) && (filename.indexOf('_modules') === -1) && (filename.indexOf('_layouts') === -1)) {
      let actualFilename = filename.replace('src/','');
      actualFilename = actualFilename.replace(/src\\/g, '');
      results.push(actualFilename);
    }
  }
  return results;
}

function generateHtmlPlugins (templateDir) {
  // Read files in template directory
  const templateFiles = getIndexPugFiles(templateDir,'.pug');

  return templateFiles.map(item => {
    // Split names and extension
    const parts = item.split('.');
    const name = parts[0]
    const extension = parts[1];

    // Create new HTMLWebpackPlugin with options
    return new HtmlWebpackPlugin({
      filename: `${name}.html`,
      template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`),
      cache: true,
      minify: false,
      hash: false,
      inject: false,
      alwaysWriteToDisk: true,
      data: siteData
    })
  })
}

const htmlPlugins = generateHtmlPlugins('./src');

const smp = new SpeedMeasurePlugin();
module.exports = smp.wrap({
  mode: process.env.NODE_ENV,
  entry:  path.resolve(__dirname, 'index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: `${assetPath}/scripts/main.js`,
    publicPath: "/"
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([
      {from:'src/_api',to:`${assetPath}/apis`},
      // {from:'src/_fonts',to:`${assetPath}/fonts`},
      {from:'src/_images',to:`${assetPath}/images`},
      // {from:'src/_videos',to:`${assetPath}/videos`},
      {from:'**/*',ignore: ['{**/\_*,**/\_*/**}','**/*.pug'],context: 'src/'}
    ]),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      $j: 'jquery'
    }),
    new MiniCssExtractPlugin({
      filename: `${assetPath}/styles/main.css`
    }),
    new HappyPack({
      id: 'html',
      loaders: [
        {
          loader: 'pug-loader',
          options: {
            pretty: true
          }
        }
      ],
      threadPool: happyThreadPool
    }),
    new HappyPack({
      id: 'js',
      loaders: [
        {
          loader: "babel-loader",
          options: {
            cacheDirectory: true
          }
        }
      ],
      threadPool: happyThreadPool
    }),
    new BrowserSyncPlugin(
      {
        files: ['styles/**/*.css', '**/*.html', '!/assets/**/*'],
        host: 'localhost',
        port: 3000,
        proxy: 'http://localhost:3002/'
      },
      {
        reload: true,
        injectCss: true
      }
    )
  ].concat(htmlPlugins),
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: "happypack/loader?id=js"
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false
            }
          },
          'postcss-loader',
          'fast-sass-loader'
        ]
      },
      {
        test: /\.pug$/,
        use: "happypack/loader?id=html"
      }
    ]
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    watchContentBase: true,
    publicPath: '/',
    hot: false,
    inline: true,
    port: 3002
  },
  resolve: {
    modules: [
      "node_modules"
    ],
    alias: {}
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        test: /\.js(\?.*)?$/i,
        extractComments: true,
        sourceMap: true
      })
    ]
  }
});

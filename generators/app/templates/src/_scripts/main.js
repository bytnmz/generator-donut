// Main javascript entry point
'use strict';

// npm module imports
// To remove if not used
import 'jquery';
import 'jquery-match-height';
import { TinyEmitter } from 'tiny-emitter';


// dev module imports
// common imports
import CustomSelect from '../_modules/common/custom-select/custom-select';
import Accordion from '../_modules/common/accordion/accordion';
import TabContent from '../_modules/common/tab-content/tab-content';
import Carousel from '../_modules/common/carousel/carousel';
// import Rte from '../_modules/common/rte/rte';

// atom imports
/**
 * Sample import
*/
import Cta from '../_modules/atoms/cta/cta';
/** */

// molecule imports


// organism imports
import Listing from '../_modules/organisms/listing/listing';


$(() => {
  // set vh unit to browser
	let vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);

	window.addEventListener("resize", () => {
  	vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty("--vh", `${vh}px`);
	});
  
  const { stickyHeader, breakpoints } = {
    stickyHeader: false,
    breakpoints: {
      tablet: 768,
      desktop: 1024,
      lgDesktop: 1280,
      XLgDesktop: 1366,
      XXLgDesktop: 1540
    }
  }

  window.emitter = new TinyEmitter();

  $('.match-height').matchHeight();

  /**
   * Sample instance of module
   */
  $('.cta').map((i, ele) => {
    const cta = new Cta($(ele));
  });
  /** */


  /** 
   * Main Start
   * First fold elements should be first
  */
  if ($('.site-header').length) {
    new SiteHeader($('.site-header'), breakpoints);
  }

  if ($('[data-carousel]').length) {
    $('[data-carousel]').map((i, ele) => {
      new Carousel($(ele), true, null, null, null);
    });
  }

  /**
   * START: Utility modules
   */
  if ($('.anchor-link').length) {
    $('.anchor-link').map((i, ele) => {
      let $this = $(ele);
      let target = $this.attr('href');

      $this.on('click', e => {
        e.preventDefault();

        const offset = stickyHeader ? $('.site-header').outerHeight() : 0;
        if ($(target).length) {
          $('html, body').animate({
            scrollTop: $(target).offset().top - offset
          });
        }
      });
    });
  }

  if ($('.custom-select').length) {
    $('.custom-select').map((i, ele) => {
      new CustomSelect($(ele));
    });
  }

  if ($('.nav-dropdown').length) {
    $('.nav-dropdown').map((i, ele) => {
      const dropdown = new NavDropdown($(ele), false);
      dropdown.init();
    });
  }

  if (!$('.sfPageEditor').length && $('.accordion-item').length) {
    $('.accordion-item').map((i, ele) => {
      // Wrap consecutive accordions with .accordion
      $(ele).not('.accordion-item+.accordion-item').each(() => {
        $(ele).nextUntil(':not(.accordion-item)').addBack().wrapAll('<section class="accordion" data-total="0" data-count="0"/>');
      });
    });

    $('.accordion').map((i, ele) => {
      // const accordionConfig = {};
      const accordion = new Accordion($(ele), false);
      accordion.init($(ele));

      if($('.accordion-item', $(ele)).length > 1) {
        accordion.initExpandCollapse($(ele));
      }
    });
  }

  if (!$('.sfPageEditor').length && $('.tab-content').length) {
    $('.tab-content').map((i, ele) => {
      const tab = new TabContent($(ele), false);
      tab.init($(ele));
    });
  }

  /** 
   * Toggle RTE module if responsive table is required
  */
  // if($('.rte').length) {
  //   $('.rte').map((i, ele) => {
  //     new Rte($(ele));
  //   });
  // }
  /** */

  /**
   * END: Utility modules
   */

  // START: AJAX Listing Modules
  if ($('.listing-layout').length) {
    $('.listing-layout').map((i, ele) => {
      const templateID = $(ele).next().attr('id');

      new Listing($(ele), `#${templateID}`);
    });
  }

  if ($('.site-footer').length) {
    new SiteFooter($('.site-footer'), breakpoints);
  }
});

'use strict';

import $ from 'jquery';
import NavDropdown from '../nav-dropdown/nav-dropdown';

export default class TabContent {
  constructor($selector, selfInit = true) {
    // Self init to be true by default unless explicitly set to false through instance argument
    if (selfInit) this.init($selector);
  }

  init($selector) {
    // Generate nav tab markup
    let _navItems = '';

    $('.tab-content__content', $selector).map((i, ele) => {
      if ($(ele).html().trim().length) {
        // do if content not empty
        const id = $(ele).attr('id');
        const title = $(ele).data('placeholder-label') ? $(ele).data('placeholder-label') : '-';
        const activeClass = i === 0 ? 'active' : '';

        const _navItem = `<li class="${activeClass}"><a href="#${id}">${title}</a></li>`;

        _navItems += _navItem;
      } else {
        // remove element if empty
        $(ele).remove();
      }
    });

    // Add tab nav markup into tab container
    $selector.prepend(`<nav class="tab-content__nav nav-dropdown"><ul>${_navItems}</ul></nav>`);

    // Add init class to tab
    $selector.addClass('tab-init');
    // set first content to active using rendered tab nav
    const $activeContent = $('.tab-content__nav ul li', $selector).first();
    $activeContent.addClass('active');
    $(`${$activeContent.find('a').attr('href')}`).addClass('active');

    // bind click handlers on generated tab items
    const $nav = $('.tab-content__nav', $selector);
    const $navItems = $('li', $nav);

    // Init nav dropdown
    const navDropdown = new NavDropdown($nav);

    $navItems.map((i, ele) => {
      const $this = $(ele);
      const $link = $('a', $this);
      const id = $link.attr('href');
      const $target = $(id);
      const label = $this.find('a').text();

      $link.on('click', (e) => {
        e.preventDefault();

        if (!$this.hasClass('active')) {
          window.history.pushState({ id }, '', `${id}`);
          this.setActive($this);
          this.switchToTab($target);

          // handle label change and hiding list
          $nav.find('.nav-dropdown__button .label').text(label);
          navDropdown.hideList();
        }
      });
    });
  }

  setActive($ele) {
    $ele.siblings('.active').removeClass('active');
    $ele.addClass('active');
  }

  switchToTab($target) {
    $target.siblings('.active').removeClass('active');
    $target.addClass('active');
  }

  closeList(label) {
    this.$nav.find('.nav-dropdown__button .label').text(label);
    this.navDropdown.hideList();
  }
}

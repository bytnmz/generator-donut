
'use strict';

export default class Checkbox {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    console.log('Checkbox');
  }
}

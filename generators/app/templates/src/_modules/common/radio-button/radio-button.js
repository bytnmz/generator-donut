'use strict';

export default class RadioButton {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    console.log('RadioButton');
  }
}

'use strict';

// import 'slick-carousel';

const _$items = new WeakMap();

export default class Accordion {
  constructor($selector, selfInit = true, config = {}) {
    this.$selector = $selector;
    _$items.set(this, $selector.find('.accordion-item'));

    // Self init to be true by default unless explicitly set to false through instance argument
    if (selfInit) this.init($selector, config);
  }

  init(config) {
    const cfg = $.extend({
      autoCollapseSiblings: false,
      scrollOffset: 0,
      scrollToPosition: false
    }, config);

    const { scrollOffset, autoCollapseSiblings, scrollToPosition } = cfg;

    _$items.get(this).map((i, ele) => {
      const $item = $(ele);
      // binding accordion operations
      const $title = $item.find('.accordion-title');

      $title.on('click', (e) => {
        e.preventDefault();

        if ($item.hasClass('expanded')) {
          this._collapseItem($item, () => {
            this.trackCount(false);
          });
        }
        else {
          if (autoCollapseSiblings) {
            /* Auto hide expanded item */
            const $activeItem = this.$selector.find('.accordion-item.expanded');

            if ($activeItem.length > 0) {
              this._collapseItem($activeItem);
            }
          }

          this._expandItem($item, () => {
            if (scrollToPosition) {
              $('html, body').animate({
                scrollTop: $title.offset().top - scrollOffset,
              });
            }

            this.trackCount();
          });
        }
      });
    });

    /* automatically expand first item only after window is fully loaded */
    // $(window).on('load', () => {
    //   this._expandItem(this.$selector.find('.accordion-item').first());
    // });
  }

  trackCount(increment = true) {
    let total = this.$selector.data('total'),
      count = this.$selector.data('count');

    if (increment) count++;
    else count--;

    this.$selector.data('count', count);

    if(count === total) {
      this.$selector.find('.expand-collapse').addClass('active').text('Collapse all');
    }
    else {
      this.$selector.find('.expand-collapse').removeClass('active').text('Expand all');
    }
  }

  _expandItem($item, callback = () => {}) {
    $item.addClass('expanding');
    $('.accordion-content', $item).slideDown(() => {
      $item.addClass('expanded');
      $item.removeClass('expanding');
      callback();

      /** Uncomment if there is a carousel in accordion,
        also uncomment import line at the top
       */
      // if($item.find('[data-carousel]').length) {
      //   $('[data-carousel]').slick('setPosition');
      // }
    });
  }

  _collapseItem($item, callback = () => {}) {
    $item.removeClass('expanded');
    $('.accordion-content', $item).stop().slideUp(() => {
      callback();
    });
  }

  expandAll() {
    _$items.get(this).map((i, item) => {
      this._expandItem($(item));
    });
  }

  collapseAll() {
    _$items.get(this).map((i, item) => {
      this._collapseItem($(item));
    });
  }

  initExpandCollapse() {
    const _expandCollapseAllButton = `<button type="button" class="expand-collapse">Expand all</button>`;

    this.$selector.prepend(_expandCollapseAllButton);
    this.$selector.data('total', $('.accordion-item', this.$selector).length);

    $('.expand-collapse', this.$selector).map((i,ele) => {
      $(ele).on('click', (e) => {
        e.preventDefault();

        if(!$(ele).hasClass('active')) {
          $(ele).addClass('active');
          $(ele).text('Collapse all');

          this.expandAll();
          this.$selector.data('count', this.$selector.data('total'));
        }
        else {
          $(ele).removeClass('active');
          $(ele).text('Expand all');

          this.collapseAll();
          this.$selector.data('count', '0');
        }
      });
    });
  }
}

'use strict';

import DateField from '../date-field/date-field';
import ComboBox from '../combo-box/combo-box';

export default class SearchFilters {
  constructor($selector, selfInit = true) {
    // Self init to be true by default unless explicitly set to false through instance argument
    if (selfInit) this.init($selector);
  }

  init($selector) {
    this._handleToggle($selector);
    this._closeButtonHandler($selector);

    if ($('.checkboxes, .radio-buttons', $selector).length) {
      $('.checkboxes, .radio-buttons', $selector).map((i, ele) => {
        this._handleOptionsToggle($(ele));
      });
    }

    this.dateFields = [];

    if ($('.date-field', this.$selector).length) {
      $('.date-field').map((i, ele) => {
        this.dateFields.push(new DateField($(ele)));
      });
    }

    if ($('.combo-box', this.$selector).length) {
      $('.combo-box').map((i, ele) => {
        new ComboBox($(ele));
      });
    }
  }

  _handleToggle($selector) {
    const $toggle = $selector.find('button.search-toggle');

    $toggle.on('click', (e) => {
      e.preventDefault();

      if (!$selector.find('.search__filters-wrapper').hasClass('active')) {
        this.openFilter($selector);
      }
    });
  }

  _closeButtonHandler($selector) {
    const $closeButton = $('.search__filters-header button', $selector);

    $closeButton.on('click', (e) => {
      e.preventDefault();

      this.closeFilter($selector);
    });
  }

  _handleOptionsToggle($options) {
    const $toggle = $('> a', $options);

    $toggle.on('click', (e) => {
      e.preventDefault();

      if (!$options.hasClass('expanded')) {
        this._expandItem($options);
      } else {
        this._collapseItem($options);
      }
    });
  }

  _expandItem($item, callback = () => {}) {
    $item.addClass('expanding');
    $('> ul', $item).slideDown(() => {
      $item.addClass('expanded');
      $item.removeClass('expanding');
      callback();
    });
  }

  _collapseItem($item, callback = () => {}) {
    $item.removeClass('expanded');
    $('> ul', $item).stop().slideUp(() => {
      callback();
    });
  }

  openFilter($selector) {
    $selector.find('.search__filters-wrapper').addClass('active');
  }

  closeFilter($selector) {
    $selector.find('.search__filters-wrapper').removeClass('active');
  }

  reset() {
    this.dateFields.map((field) => {
      field.reset();
    });
  }
}

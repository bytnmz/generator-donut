'use strict';

const _totalPages = new WeakMap();
const _page = new WeakMap();
const _$list = new WeakMap();

export default class Pagination {
  constructor($selector) {
    _totalPages.set(this, 1);
    _page.set(this, 1);
    _$list.set(this, $selector);
  }

  update(page, totalPages, callback) {
    _totalPages.set(this, totalPages);
    _page.set(this, page);

    this._render(callback);
  }

  _render(callback = () => {}) {
    let totalPages = _totalPages.get(this);
    let page = parseInt(_page.get(this));

    if (totalPages < 2) {
      _$list.get(this).html('');
      _$list.get(this).next().html('');
      return;
    }

    const centerPadding = 2;
    // let paginationSize = centerPadding * 2 + 1;
    let _html = '';
    if (page > 1) {
      _html += `
      <li class="pagination-prev">
        <a href="#", title="Go to page ${ page - 1 }" data-page="${ page - 1 }">Previous</a>
      </li>
      `;
    }

    _html += `
    <li class="${ (page == 1) ? 'active' : ''}">
      <a href="#" title="Go to page 1" data-page="1">1</a>
    </li>
    `;

    let start = (page - centerPadding >= 2) ? page - centerPadding : 2;
    let end = (page + centerPadding >= totalPages - 1) ? totalPages - 1 : page + centerPadding;

    if (page - centerPadding > 2 && start > 2) {
      _html += `
      <li class="pagination-ellipsis">
        <span>...</span>
      </li>
      `;
    }

    let i = start;
    while (i <= end) {
      _html += `
      <li class="${ (i == page) ? 'active' : ''}">
        <a href="#" title="Go to page ${i}" data-page="${i}">${i}</a>
      </li>
      `;
      i++;
    }

    if (page + centerPadding < totalPages - 1 && end < totalPages - 1) {
      _html += `
      <li class="pagination-ellipsis">
        <span>...</span>
      </li>
      `
    }

    _html += `
    <li class="${ (page == totalPages) ? 'active' : ''}">
      <a href="#" title="Go to page ${ totalPages }" data-page="${ totalPages }">${ totalPages }</a>
    </li>
    `;

    if (page < totalPages) {
      _html += `
      <li class="pagination-next">
        <a href="#" title="Go to page ${ page + 1 }" data-page="${ page + 1 }">Next</a>
      </li>
      `;
    }

    _$list.get(this).html(_html);
    _$list.get(this).next().html(this._mobileHtml());

    callback();
  }

  _mobileHtml() {
    let totalPages = _totalPages.get(this);
    let page = parseInt(_page.get(this));

    let prevPage = (page > 1) ? page - 1 : 1;
    let nextPage = (page < totalPages) ? page + 1 : totalPages;

    let _html = `
      <div class="m-pagination-prev m-pagination-ctrl ${ (page == 1) ? 'disabled' : ''}">
        <a href="#" data-page="${ prevPage }">Previous</a>
      </div>
    `;

    _html += `
      <div class="m-pagination-number">
        <span class="current-page">${ page }</span>
        <span class="slash">/</span>
        <span class="total-page">${ totalPages }</span>
      </div>
    `;

    _html += `
      <div class="m-pagination-next m-pagination-ctrl ${ (page == totalPages) ? 'disabled' : ''}">
        <a href="#" data-page="${ nextPage }">Next</a>
      </div>
    `;

    return _html;
  }
}

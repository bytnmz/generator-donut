'use strict';

import TableResponsive from '../table-responsive/table-responsive';

export default class Rte {
  constructor($selector = $('.rte'), selfInit = true) {
    this.$selector = $selector;

    if (selfInit) this.init();
  }

  init() {
    if($('table', this.$selector).length){
			$('table', this.$selector).map((i, ele) => {
        new TableResponsive($(ele));
      });
    }
  }
}

'use strict';

import ListingBase from '../listing-base/listing-base';
import Pagination from '../pagination/pagination';

export class AjaxListing extends ListingBase {
  constructor($selector, templateID) {
    super();

    this.$filters = $('.search__filters', $selector);
    const $searchForm = $('.search__form', $selector);
    const $wrapper = $('.search__results', $selector);
    const $container = $('.search__results-list', $selector);
    const $pagination = $('.pagination', $selector);
    const pagination = new Pagination($pagination);

    /* 1. Setup basic config */
    this.template = $(templateID).html();
    this.endpoint = $selector.data('endpoint');

    // Get all fields
    const allFields = this.getAllFields($selector);

    // Generate parameters using fields
    this.parameters = this.getParameters(allFields);

    /* 2. Setup the listener (the following is available in the ListingBase, can reuse) */
    this.setupListeners(allFields);

    $searchForm.on('submit', e => {
      e.preventDefault();
      this.parameters = this.getParameters(allFields, true, null);

      this.getData();
      this.updateURL();
      this.closeFilterPopup($('.search__filters-wrapper.active', this.$filters));
    });

    $('button[type="submit"]', $selector).map((i, ele) => {
      this._addSubmitButtonListener($(ele), allFields);
    });

    $('button[type="reset"]', $selector).map((i, ele) => {
      this.setupReset($(ele), allFields);
    });

    let totalPages = 1;
    this.setupPagination('page');

    /* 3. Define the callback functions */
    this.beforeGetData = () => {
      // e.g. Can show loading gif here
      $wrapper.addClass('loading');
    };

    this.getDataCallback = (res) => {
      // e.g. Hide loading gif
      if (res.totalItems == 0) {
        $wrapper.removeClass('has-results');
      } else {
        $wrapper.addClass('has-results');
      }

      $wrapper.removeClass('loading');

      // START: Handle scroll to top of listing
      let offset = $('.site-header').outerHeight();

      if ($(window).scrollTop() + offset > $selector.offset().top) {
        $('html, body').animate({
          scrollTop: $selector.offset().top - offset
        });
      }
      // END

      /* 4. Call this function to render the template into the DOM.
            Define {$container} where you want to insert DOM.
        */
      this.renderTemplate(res.items, $container);
      this.updateTotalResults(res.totalItems, $selector);

      totalPages = res.totalPages;
      this.reInitPagination(
        'page',
        pagination,
        $pagination,
        this.parameters.page,
        totalPages
      );
    };

    /* 5. Define callback for window history */
    window.onpopstate = (e) => {
      for (let key in e.state) {
        this.parameters[key] = e.state[key];
      }

      this.updateViews(allFields);

      this.reInitPagination(
        'page',
        pagination,
        $pagination,
        this.parameters.page,
        totalPages
      );

      this.getData();
    };

    /* 6. Call first load */
    this.getData();
    this.updateURL(true);
  }
}

'use strict';

import 'slick-carousel';

export default class Carousel {
  constructor($selector, selfInit = true, slickInit, before, after) {
    this.$selector = $selector;
    this.desktop = $(window).width() >= 1024 ? true : false;
    if(selfInit) this.init(slickInit, before, after);
  }

  init(slickInit = () => {}, before = (e, slick, currentSlide, nextSlide) => {}, after = (e, slick, currentSlide) => {}, resetOnResize) {
    const $arrows = this.$selector.parent().find('[data-carousel-arrows]').length ? this.$selector.parent().find('[data-carousel-arrows]') : this.$selector;

    let options = {};

    const defaultOptions = {
      rows: 0,
      prevArrow: '<button type="button" class="slick-prev slick-arrow"><span class="sr-only">Previous</span><span class="icon-chevron-left"></span></button>',
      nextArrow: '<button type="button" class="slick-next slick-arrow"><span class="sr-only">Next</span><span class="icon-chevron-right"></span></button>',
      appendArrows: $arrows
    };
    
    options = { ...defaultOptions, ...this.$selector.data('options') };

    if(this.$selector.parent().find('[data-carousel-dots]').length) {
      const $dots = this.$selector.parent().find('[data-carousel-dots]');
      const dotOption = {
        appendDots: $dots
      };

      options = { ...options, ...dotOption };
    }

    if(this.$selector.data('navfor')) {
      const $container = this.$selector.closest('section');
      const $navFor = $(`${this.$selector.data('navfor')}`, $container);

      const navForOption = {
        asNavFor: $navFor
      }

      options = { ...options, ...navForOption };
    }

    if(this.$selector.parent().find('[data-carousel-pager]').length) {
      this.$selector.on('init reInit', (event, slick, currentSlide, nextSlide) => {
        this.$selector.parent().find('[data-carousel-pager]').text(`${(currentSlide ? currentSlide : 0) + 1} / ${slick.slideCount}`);
      });
      this.$selector.on('beforeChange', (event, slick, currentSlide, nextSlide) => {
        this.$selector.parent().find('[data-carousel-pager]').text(`${(nextSlide ? nextSlide : 0) + 1} / ${slick.slideCount}`);
      });
    }

    this.$selector.on('init', (e, slick) => {
      this._onPrint(this.$selector);
      if (slickInit) slickInit();
    });

    this.$selector.slick(options);

    if (before) this.$selector.on('beforeChange', before);

    if (after) this.$selector.on('afterChange', after);
  }

  _onPrint($carousel){
    let beforePrint = function () {
      console.log('Functionality to run before printing.');
      $carousel.slick('setPosition');
    };

    let afterPrint = function () {
      console.log('Functionality to run after printing');
      $carousel.slick('setPosition');
    };

    if (window.matchMedia) {
      let mediaQueryList = window.matchMedia('print');

      mediaQueryList.addListener(function (mql) {
        if (mql.matches) {
          beforePrint();
        } else {
          afterPrint();
        }
      });
    }

    window.onbeforeprint = beforePrint;
    window.onafterprint = afterPrint;
  }
}
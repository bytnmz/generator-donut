'use strict';

export default class Breadcrumb {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    console.log('Breadcrumb');
  }
}

'use strict';

export default class Jumbotron {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    console.log('Jumbotron');
  }
}

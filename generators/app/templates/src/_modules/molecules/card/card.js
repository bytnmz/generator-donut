'use strict';

export default class Card {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    console.log('Card');
  }
}

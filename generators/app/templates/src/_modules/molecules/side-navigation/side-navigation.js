'use strict';

export default class SideNavigation {
  constructor($selector, selfInit = true) {
    this.$selector = $selector;

    if(selfInit) this.init();
  }

  init() {
    console.log('SideNavigation');
  }
}

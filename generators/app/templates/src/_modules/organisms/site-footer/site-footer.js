'use strict';

export default class SiteFooter {
  constructor($selector, breakpoints, selfInit = true) {
    this.$selector = $selector;

    if (selfInit) this.init(breakpoints);
  }

  init(breakpoints) {
    const $scrollTopBtn = $('.btn-scroll-top', this.$selector);

    $scrollTopBtn.on('click', e => {
      e.preventDefault();

      $('html, body').animate({
        scrollTop: 0
      });
    });
  }
}

'use strict';

const Generator = require('yeoman-generator');

module.exports = class extends Generator {
  initializing() {
    let layouts = this.config.get('layouts');

    if(layouts === undefined){
      layouts = [];
      layouts.push(this.arguments[0]);
    }
    else {
      layouts.push(this.arguments[0]);
    }

    this.config.set('layouts', layouts);
    
    this.name = 'no-name';
    if (this.arguments[0]) {
      this.name = this.arguments[0];
    }

    this.layout = 'base';
    if (this.options.layout) {
      this.layout = this.options.layout;
    }
  }

  writing() {
    const templateData = {
      name: this.name,
      layout: this.layout
    };

    this.fs.copyTpl(
      this.templatePath('layout.pug'),
      this.destinationPath(`src/_layouts/${this.arguments}.pug`),
      templateData
    );
  }
};
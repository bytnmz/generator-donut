'use strict';

export default class <%= pascalName %> {
  constructor($selector, selfInit = true) {
    this.name = '<%= name %>';
    console.log('<%= name %> module');

    if(selfInit) this.init($selector);
  }

  init($selector) {

  }
}

'use strict';

const Generator = require('yeoman-generator');
const chalk = require('chalk');
const _ = require('lodash');

module.exports = class extends Generator {
  initializing() {
    this.name = '';
    if (this.arguments[0]) {
      this.name = this.arguments[0].split('/').slice(-1)[0];
    }

    if(this.name.length === 0) {
      this.log(
        chalk.red(
          'Error: Please give the module a name.'
        )
      );
      this.abort = true;
    }

    this.atomic = '';
    if(this.options.atomic) {
      this.atomic = this.options.atomic;
    }

    this.modulePath = 'src/_modules';
    if(this.atomic.length) {
      if (['atom', 'molecule', 'organism'].indexOf(this.atomic) > -1) {
        this.modulePath += `/${this.atomic}s`;
      }
      else {
        this.log(
          chalk.red(
            'Error: This option only accepts the following 3 options: "atom","molecule" or "organism".'
          )
        );
        this.abort = true;
      }
    }
  }

  writing() {
    if (this.abort) {
      return;
    }

    const templateData = {
      name: this.name,
      pascalName: _.upperFirst(_.camelCase(this.name))
    };

    this.fs.copyTpl(
      this.templatePath('module.pug'),
      this.destinationPath(`${this.modulePath}/${this.name}/${this.name}.pug`),
      templateData
    );

    this.fs.copyTpl(
      this.templatePath('module.scss'),
      this.destinationPath(`${this.modulePath}/${this.name}/${this.name}.scss`),
      templateData
    );

    this.fs.copyTpl(
      this.templatePath('module.js'),
      this.destinationPath(`${this.modulePath}/${this.name}/${this.name}.js`),
      templateData
    );
  }
};